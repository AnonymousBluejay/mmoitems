package net.Indyuce.mmoitems.api.interaction.weapon.untargeted.staff.options;

import org.bukkit.Color;
import org.bukkit.Particle;

import java.util.HashMap;

public class SpiritOptions {

    private final String config;
    private Particle particle = null;
    private Color color = null;
    private float speed = 0;
    private int size = 1;

    //SpiritOptions created through the constructors will not be saved to the cache
    public SpiritOptions(String config) {
        this.config = config;
        generate();
    }
    //To make it easier on programmers
    public SpiritOptions(String config, Particle particle, Color color) {
        this.config = config;
        this.particle = particle;
        this.color = color;
    }
    public void generate() {
        for (String str : config.split(";")) {
            if(str.contains("=")) {
                String key = str.substring(0, str.indexOf("="));
                String value = str.substring(str.indexOf("=") + 1);
                //Example: p=REDSTONE;rgb=255-255-255
                //Not all values need to be used;
                try {
                    switch (key) {
                        case "p":
                        case "particle":
                            particle = parseParticle(value, Particle.CRIT);
                            break;
                        case "color":
                        case "rgb":
                            color = parseRGB(value);
                            break;
                        case "s":
                        case "speed":
                            speed = (float) Double.parseDouble(value);
                            break;
                        case "size":
                            size = Integer.parseInt(value);
                            break;
                    }
                } catch(NumberFormatException ignored) {

                }
                //Separate particles, hit particles etc
                //Shoot sounds
                //Gradients? Not sure how well it would be used p=REDSTONE;gradient=255-255-255,110-250-250,5,linear,smooth;size=1;s=0
            }
        }
    }

    public static SpiritOptions get(String config) {
        if(!CACHE.containsKey(config)) {
            CACHE.put(config, new SpiritOptions(config));
        }
        return CACHE.get(config);
    }

    //Particle uses the particle enum names defined by minecraft
    public static Particle parseParticle(String particle, Particle defaultParticle) {
        try {
            return Particle.valueOf(particle.toUpperCase());
        } catch (IllegalArgumentException | NullPointerException e) {
            return defaultParticle;
        }
    }
    //RGB uses the format of R-G-B
    public static Color parseRGB(String color) {
        String[] rgb = color.split("-");
        try {
            int r = Integer.parseInt(rgb[0]);
            int g = Integer.parseInt(rgb[1]);
            int b = Integer.parseInt(rgb[2]);
            return Color.fromRGB(r, g, b);
        } catch(NumberFormatException e) {
            return Color.WHITE;
        }
    }

    private static final HashMap<String, SpiritOptions> CACHE = new HashMap<>();

    public String getConfig() {
        return config;
    }

    public Particle getParticle() {
        return particle;
    }
    public Particle getParticle(Particle defaultIfNull) {
        return particle == null ? defaultIfNull : particle;
    }
    public Particle getParticle(SpiritOptions defaultIfNull) {
        return particle == null ? defaultIfNull.getParticle(Particle.REDSTONE) : particle;
    }

    public Color getColor() {
        return color;
    }
    public Color getColor(Color defaultIfNull) {
        return particle == null ? defaultIfNull : color;
    }
    public Color getColor(SpiritOptions defaultIfNull) {
        return color == null ? defaultIfNull.getColor(Color.RED) : color;
    }

    public float getSpeed() {
        return speed;
    }
    public float getSize() {
        return size;
    }
}
