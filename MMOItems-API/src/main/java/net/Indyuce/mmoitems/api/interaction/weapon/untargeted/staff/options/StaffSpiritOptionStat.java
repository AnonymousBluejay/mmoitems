package net.Indyuce.mmoitems.api.interaction.weapon.untargeted.staff.options;

import io.lumine.mythic.lib.api.item.ItemTag;
import io.lumine.mythic.lib.version.VersionMaterial;
import net.Indyuce.mmoitems.MMOItems;
import net.Indyuce.mmoitems.api.item.build.ItemStackBuilder;
import net.Indyuce.mmoitems.gui.edition.EditionInventory;
import net.Indyuce.mmoitems.stat.data.StringData;
import net.Indyuce.mmoitems.stat.type.StringStat;
import org.jetbrains.annotations.NotNull;

public class StaffSpiritOptionStat extends StringStat {

    public StaffSpiritOptionStat() {
        super("STAFF_SPIRIT_OPTIONS", VersionMaterial.BONE_MEAL.toMaterial(), "Staff Spirit Options",
                new String[]{
                        "Some extra options for staff spirit",
                        "Ex. &eparticle=REDSTONE;rgb=255-255-255;s=0;size=1",
                        "&3XRAY_SPIRIT &7rgb(color)",
                        "&3MANA_SPIRIT &7particle(p),rgb(color),speed(s),size",
                        "Find list of particles here: ",
                        "https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Particle.html"
                },
                new String[]{"staff", "wand"});
    }

    @Override
    public void whenInput(@NotNull EditionInventory inv, @NotNull String message, Object... info) {
        try {
            //Staff spirit can be entered regularly or in the format of option1=value1;option2=value2... to add extra options
            inv.getEditedSection().set("staff-spirit-options", message);
            inv.registerTemplateEdition();
            inv.getPlayer().sendMessage(MMOItems.plugin.getPrefix() + "Staff Spirit Options changed to " + message + ".");
        } catch (IllegalArgumentException exception) {
            throw new IllegalArgumentException(exception.getMessage() + " (Something went wrong).");
        }
    }

    @Override
    public void whenApplied(@NotNull ItemStackBuilder item, @NotNull StringData data) {
        item.addItemTag(new ItemTag("MMOITEMS_STAFF_SPIRIT_OPTIONS", data.toString()));
    }
}
